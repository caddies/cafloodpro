/*
    
Copyright (c) 2024 Centre for Water Systems,
                   University of Exeter

This file is part of cafloodpro.

cafloodpro is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

// Add the volume of water from an inflow event to the water depth.
CA_FUNCTION addInflow(CA_GRID grid, CA_CELLBUFF_REAL_IO WD, CA_CELLBUFF_STATE_I MASK, 
		      CA_GLOB_REAL_I volume)
{
  // Initialise the grid
  CA_GRID_INIT(grid);

  // Retrive the water depth 
  CA_REAL  wd    = caReadCellBuffReal(grid,WD,0);

  // Retrive the area of the main cell.
  CA_REAL  area = caArea(grid,0);

  // Read Mask.
  CA_STATE mask  = caReadCellBuffState(grid,MASK,0);

  // Read bit 0  (false the main cell has nodata)
  CA_STATE bit0  = caReadBitsState(mask,0,1);

  // If the main cell has no data. Do
  // nothing.
  //if(bit0 == 0)
    //return;

  // Update the water depth.
  caWriteCellBuffReal(grid,WD,wd+(volume/area));
}
