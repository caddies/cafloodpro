/*
    
Copyright (c) 2024 Centre for Water Systems,
                   University of Exeter

This file is part of cafloodpro.

cafloodpro is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

// Compute the outflow from the water depth using WICA2D model

#ifdef cl_nv_pragma_unroll
#pragma OPENCL EXTENSION cl_nv_pragma_unroll : enable
#endif

CA_FUNCTION outflowWICA2D(CA_GRID grid, CA_EDGEBUFF_REAL_I TOT, CA_EDGEBUFF_REAL_IO OUTF1, 
			  CA_CELLBUFF_REAL_I ELV, CA_CELLBUFF_REAL_I WD, 
			  CA_CELLBUFF_STATE_I MASK, CA_ALARMS_O ALARMS, 
			  CA_TABLE_REAL_I IROUGH, CA_GLOB_STATE_I spos, CA_GLOB_STATE_I epos,
			  CA_GLOB_REAL_I ignore_wd, CA_GLOB_REAL_I tol_delwl,
			  CA_GLOB_REAL_I dt, CA_GLOB_REAL_I prev_dt)
{					       
  // Initialise the grid
  CA_GRID_INIT(grid);
    
  // Read Mask.
  CA_STATE mask  = caReadCellBuffState(grid,MASK,0);

  // Retrieve the water depth of the main cell.
  CA_REAL  wdmain = caReadCellBuffReal(grid,WD,0);
  
  // Read bit 0  (false the main cell has nodata)
  CA_STATE bit0  = caReadBitsState(mask,0,1);

  // Retrieve the 0 of the spatial roughness.
  CA_STATE idx = caReadBitsState(mask,spos,epos);

  // If the main cell has no data (bit0 is false ) or there is no
  // water do not compute the cell.
  if(!(bit0 == 0 ||  wdmain < ignore_wd))
  {
    // Create an array which will contain various water values during the
    // computation for each neighbourh cell, in the following order:
    // 1) WATER depth
    // 2) WATER level
    CA_ARRAY_CREATE(grid, CA_REAL, WATER,caNeighbours+1);

    // Create an array wich will contain vairous weight value during the
    // computation for each eighbour cell, in the follwoing order:
    // 1) Elevation
    // 2) 'Inertial' Weight.
    // Attention, for the majority of the code whe we talk about weight
    // we mainly inted the numerator of the weight fraction.
    CA_ARRAY_CREATE(grid, CA_REAL, WEIGHT, caNeighbours+1);  

    // Create the array which will contain the total water fluxes on
    // the edges for the last update step.
    // This coontains the 'inertial' weight
    CA_ARRAY_CREATE(grid, CA_REAL, PREVFLUXES, caEdges+1);

    // Read the water depth of the neighbourhood cells (store temporarily into WATER).
    caReadCellBuffRealCellArray(grid,WD,WATER);

    // Read the elevation of the neighbourhood cells, (stored temporarly in WEIGHT).
    caReadCellBuffRealCellArray(grid,ELV,WEIGHT);

    // Read the total fluxes of the main cell of update step so far.
    caReadEdgeBuffRealEdgeArray(grid, TOT,0, PREVFLUXES);

    // Reteieve the inverse roughness from the table.
    CA_REAL irough = caReadTableReal(grid,IROUGH,idx);
      
    // Compute the water level of the cells since WATER and WEIGHT
    // contein respectively the water detph and elevation.
#pragma unroll
    for(int k=0; k<=caNeighbours; ++k)
      WATER[k]+=WEIGHT[k];    

    // The total inertial flux (weight)
    CA_REAL totalinw = 0.0;
    
    // Find the 'positive' weight of each edge, i.e. positive flux
    // computed using the inertial formula.    
    for(int k=1; k<=caNeighbours; ++k)
    {   
      // Change that each outflux is
      // positive and each influx is negative.  
      if(k<=caUpdateEdges(grid))
	PREVFLUXES[k] = PREVFLUXES[k];
      else
	PREVFLUXES[k] = -PREVFLUXES[k];
      
      // WEIGHT[0] is the elevation of the main cell.
      // WEIGHT[k] is still the elavation
      CA_REAL delwl  = caMinReal(WATER[0] - WATER[k], 50.0);
      CA_REAL Hf    = caMaxReal(WATER[0],WATER[k]) - caMaxReal(WEIGHT[0], WEIGHT[k]);

      CA_REAL dx     = caLength(grid,0,k);
      CA_REAL dist   = caDistance(grid,k);
      CA_REAL A      = Hf*dx;
      CA_REAL Qp     = PREVFLUXES[k]/(prev_dt);

      // COMMON VALUES.
      CA_REAL R_pow = caPowReal(Hf,(4.0/3.0));
      
      CA_REAL n  = 1/irough;
      //CA_REAL Qn = Qp + 9.81*dt*((A*(delwl/dist))-((n*n*Qp*Qp)/(R_pow*A)));
      CA_REAL gdt  = 9.81*dt;
      CA_REAL Qn   = (Qp + gdt*(A*(delwl/dist)))/ (1+gdt*((n*n*caAbsReal(Qp))/(R_pow*A)));
      CA_REAL flux = Qn * dt;  

      // Retrive only positive flux.
      WEIGHT[k]=(flux>0)? flux : 0.0;

      // Add positive flux to total.
      totalinw+=WEIGHT[k];
    }

    // At this point if the total inertial weight is zero then there
    // is not outflow. Skip this cell
    if(!(totalinw<=0.0))
    {
      // Compute the total amount of output water volume that we can move
      // from the main cell. This is the minimum between.
      // 1) the water level in the cell.
      // 2) the total output flux computed using the inertial formual.
      CA_REAL outwv = caMinReal(totalinw,wdmain*caArea(grid,0));
      
      // Compute volume outflow from the main cell using the eventual
      // positive weight.
      for(int k=1; k<=caNeighbours; ++k)
      {
	// Consider only cell with outflow.
	if(WEIGHT[k]>0.0)
	{
	  // Compute flux using the weight system 
	  CA_REAL flux = outwv*(WEIGHT[k]/totalinw);
	  
	  // If the edge is one of the edges that can be updated without
	  // overwriting the buffer, the outflux is positive otherwise is
	  // negative.
	  if(k > caUpdateEdges(grid))
	    flux=-flux;
	  
	  // Set flux into the edge buffer.
	  caWriteEdgeBuffReal(grid,OUTF1,k,flux);
	}
      }
  
    // If the code reach here mean that there is an outflux.  Check if
    // the cell is in the border of the box and if it is the case set an
    // alarm.
    if(caBoxStatus(grid)>0)
      caActivateAlarm(grid, ALARMS,0);


    } // No outflow.
  } // Now water or no data cell.

}
